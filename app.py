import scrap_result
from flask import Flask, render_template, request

app = Flask(__name__)


@app.route("/", methods=["POST", "GET"])
def index():
    return render_template("index.html")


@app.route("/result/", methods=["POST", "GET"])
def result():
    try:
        search_keyword = request.form.get("search_keyword")
        scrap_result.connect_to_link()
        scrap_result.use_search_engine(search_keyword)
        search_result = scrap_result.scrap_search_results()
        list_of_results = []
        for company in search_result:
            result_dict = {
                "company_title": scrap_result.get_company_title(company),
                "company_address": scrap_result.get_company_address(company),
                "company_about": scrap_result.get_company_about(company),
                "company_phone_number": scrap_result.get_itme_phone_number(company),
                "company_website": scrap_result.get_company_website(company),
                "company_email_link": scrap_result.get_company_mail_link(company),
                "company_rating": scrap_result.get_company_rating(company),
            }
            list_of_results.append(result_dict)
        scrap_result.close_opened_taps()
        return render_template(
            "result.html",
            list_of_results=list_of_results,
            search_keyword=search_keyword,
        )
    except Exception as e:
        print(e)
        return render_template("index.html", search_keyword=search_keyword)


if __name__ == "__main__":
    app.run(debug=True)
