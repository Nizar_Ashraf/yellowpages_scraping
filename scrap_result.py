from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options

browser = None


def connect_to_link():
    global browser
    options = Options()
    options.add_argument("--headless")
    browser = webdriver.Chrome(options=options)
    browser.get("https://yellowpages.com.eg/en")
    try:
        alert = WebDriverWait(browser, 10).until(EC.alert_is_present())
        alert.accept()
    except:
        pass


def use_search_engine(search_key_word):
    if search_key_word:
        search_btn = browser.find_element(By.CLASS_NAME, "search-submit-btn")
        search_box = WebDriverWait(browser, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME, "what_original_top"))
        )
        search_box.clear()
        search_box.send_keys(search_key_word)
        search_btn.click()
    else:
        close_opened_taps()


def scrap_search_results():
    search_result = WebDriverWait(browser, 10).until(
        EC.presence_of_all_elements_located((By.CLASS_NAME, "item-details"))
    )
    return search_result[:3]
    # change this number to specify how many search result you want to get#


def get_company_title(company):
    try:
        company_title = company.find_element(By.CLASS_NAME, "item-title")
        return company_title.text
    except:
        return "empty"


def get_company_address(company):
    try:
        company_address = company.find_element(By.CLASS_NAME, "address-text")
        return company_address.text
    except:
        return "empty"


def get_company_about(company):
    try:
        company_about = company.find_element(By.CLASS_NAME, "item-aboutUs")
        return company_about.text
    except:
        return "empty"


def get_itme_phone_number(company):
    try:
        company_phone_link = company.find_element(By.CSS_SELECTOR, "span.call-us-click")
        link = company_phone_link.get_attribute("data-tooltip-phones")
        options = Options()
        options.add_argument("--headless")
        driver = webdriver.Chrome(options=options)
        driver.get(f"https://yellowpages.com.eg{link}")
        phone_number_list = driver.find_element(By.TAG_NAME, "body")
        company_phone_number = []
        for value in phone_number_list.text.strip("[]").split(","):
            number = value.strip('""').replace('"]', "")
            if number:
                company_phone_number.append(number)
        return company_phone_number[0]
    except:
        return "empty"


def get_company_website(company):
    try:
        company_website = company.find_element(By.CLASS_NAME, "website").get_attribute(
            "href"
        )
        return company_website
    except:
        return "empty"


def get_company_rating(company):
    try:
        company_rating = company.find_element(By.CLASS_NAME, "rating")
        return company_rating.text
    except:
        return "empty"


def get_company_mail_link(company):
    try:
        company_email_link = company.find_element(
            By.CLASS_NAME, "tab-mail"
        ).get_attribute("href")
        return company_email_link
    except:
        return "empty"


def close_opened_taps():
    browser.quit()
